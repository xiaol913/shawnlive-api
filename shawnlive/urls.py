"""shawnlive URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.views.static import serve
from rest_framework.routers import DefaultRouter
from xadmin import site

from shawnlive.settings import MEDIA_ROOT
from about.views import AboutMeViewSet
from about.views import get_result
from blogs.views import BlogViewSet
from blogs.views import CommentViewSet
from photos.views import PhotoViewSet
from projects.views import ProjectsViewSet

router = DefaultRouter()

router.register(r'about', AboutMeViewSet, base_name='关于我')
router.register(r'blog', BlogViewSet, base_name='博客')
router.register(r'comment', CommentViewSet, base_name='评论')
router.register(r'photos', PhotoViewSet, base_name='照片')
router.register(r'projects', ProjectsViewSet, base_name='项目')

urlpatterns = [
    path('admin/', site.urls),

    path('media/<path:path>', serve, {"document_root": MEDIA_ROOT}),

    path('', include(router.urls)),

    path('ueditor/', include('DjangoUeditor.urls')),

    path('sudoku/', get_result),  # 数独
]
