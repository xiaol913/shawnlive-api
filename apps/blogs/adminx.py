# _*_ coding=utf8 _*_
from xadmin import sites

from .models import Blog
from .models import Comment


@sites.register(Blog)
class BlogAdmin(object):
    model_icon = 'fa fa-photo'
    list_display = ['title', 'create_date']
    style_fields = {
        'content': 'ueditor'
    }


@sites.register(Comment)
class CommentAdmin(object):
    model_icon = 'fa fa-photo'
    list_display = ['name', 'create_date']
