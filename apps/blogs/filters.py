# _*_ coding=utf8 _*_
import django_filters

from .models import Comment


class CommentFilter(django_filters.rest_framework.FilterSet):
    """
    评论过滤
    """

    class Meta:
        model = Comment
        fields = ['blog', ]
