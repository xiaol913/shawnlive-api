from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, mixins
from rest_framework.response import Response

from .models import Blog
from .models import Comment
from .serializer import BlogSerializer
from .serializer import CommentSerializer
from .filters import CommentFilter

# Create your views here.


class BlogViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.RetrieveModelMixin):
    """
    博客
    """
    queryset = Blog.objects.all().order_by('-create_date')
    serializer_class = BlogSerializer

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.view += 1
        instance.save()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class CommentViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin):
    """
    评论
    """
    queryset = Comment.objects.all().order_by('-create_date')
    serializer_class = CommentSerializer
    filter_backends = (DjangoFilterBackend, )
    filter_class = CommentFilter
