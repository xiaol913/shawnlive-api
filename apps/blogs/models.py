from datetime import datetime

from django.db import models
from DjangoUeditor.models import UEditorField


# Create your models here.


class Blog(models.Model):
    """
    博客文章
    """
    BLOG_TYPE = (
        (1, 'python'),
        (2, 'web'),
        (3, 'server'),
        (4, 'others'),
    )

    title = models.CharField(max_length=100, verbose_name='标题', help_text='标题')
    create_date = models.DateTimeField(default=datetime.now, verbose_name='发表时间', help_text='发表时间')
    content = UEditorField(default='', verbose_name='内容', help_text='内容', width=1000, height=300, imagePath='blog/img/',
                           null=True, blank=True)
    label = models.IntegerField(choices=BLOG_TYPE, verbose_name='博客类型', help_text='博客类型')
    view = models.IntegerField(verbose_name='阅读量', help_text='阅读量', null=True, blank=True)

    class Meta:
        verbose_name = '博客文章'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.title


class Comment(models.Model):
    """
    评论
    """
    name = models.CharField(max_length=20, verbose_name='姓名', help_text='姓名')
    content = models.TextField(verbose_name='评论', help_text='评论')
    create_date = models.DateTimeField(default=datetime.now, verbose_name='发表时间', help_text='发表时间')
    blog = models.ForeignKey(Blog, verbose_name='博文', help_text='博文', related_name='comment', on_delete=models.CASCADE)

    class Meta:
        verbose_name = '评论'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name
