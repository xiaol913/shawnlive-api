# _*_ coding=utf8 _*_
from rest_framework import serializers

from .models import Blog
from .models import Comment


class CommentSerializer(serializers.ModelSerializer):
    """
    评论序列化
    """

    class Meta:
        model = Comment
        fields = '__all__'


class BlogSerializer(serializers.ModelSerializer):
    """
    博客序列化
    """
    comment = CommentSerializer(many=True)

    class Meta:
        model = Blog
        fields = ['id', 'title', 'create_date', 'content', 'label', 'comment', 'view']
