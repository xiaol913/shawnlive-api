from django.apps import AppConfig


class PhotosConfig(AppConfig):
    name = 'photos'
    verbose_name = '照片'
