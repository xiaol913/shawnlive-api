# _*_ coding=utf8 _*_
from xadmin import sites

from .models import Photos


@sites.register(Photos)
class PhotosAdmin(object):
    model_icon = 'fa fa-photo'
    list_display = ['title', 'img']
