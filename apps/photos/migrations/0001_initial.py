# Generated by Django 2.1 on 2018-08-29 10:43

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Photos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(help_text='标题', max_length=50, verbose_name='标题')),
                ('dec', models.CharField(help_text='描述', max_length=100, verbose_name='描述')),
                ('img', models.ImageField(help_text='图片', upload_to='', verbose_name='图片')),
            ],
            options={
                'verbose_name': '图片',
                'verbose_name_plural': '图片',
            },
        ),
    ]
