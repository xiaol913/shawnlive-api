# _*_ coding=utf8 _*_
from rest_framework import serializers

from .models import Photos


class PhotoSerializer(serializers.ModelSerializer):
    """
    照片
    """

    class Meta:
        model = Photos
        fields = '__all__'
