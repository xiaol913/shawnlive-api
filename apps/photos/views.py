from rest_framework import viewsets, mixins

from .models import Photos
from .serializer import PhotoSerializer


# Create your views here.


class PhotoViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    """
    list:
        照片列表
    """
    queryset = Photos.objects.all()
    serializer_class = PhotoSerializer
