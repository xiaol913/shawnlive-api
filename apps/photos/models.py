from django.db import models

# Create your models here.


class Photos(models.Model):
    """
    照片
    """
    title = models.CharField(max_length=50, verbose_name='标题', help_text='标题')
    dec = models.CharField(max_length=100, verbose_name='描述', help_text='描述')
    img = models.ImageField(verbose_name='图片', help_text='图片')

    class Meta:
        verbose_name = '图片'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.title
