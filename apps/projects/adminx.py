# _*_ coding=utf8 _*_
from xadmin import sites

from .models import Projects


@sites.register(Projects)
class PhotosAdmin(object):
    model_icon = 'fa fa-photo'
    list_display = ['name', 'url']
    style_fields = {
        'desc': 'ueditor'
    }
