from django.db import models
from DjangoUeditor.models import UEditorField


# Create your models here.

class Projects(models.Model):
    """
    项目
    """
    name = models.CharField(max_length=20, verbose_name='项目名称', help_text='项目名称')
    intro = models.CharField(max_length=100, verbose_name='项目简介', help_text='项目简介')
    desc = UEditorField(default='', verbose_name='项目描述', help_text='项目描述', width=1000, height=300, null=True,
                        blank=True)
    url = models.URLField(verbose_name='项目URL', help_text='项目URL')

    class Meta:
        verbose_name = '项目'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name
