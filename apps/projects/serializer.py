# _*_ coding=utf8 _*_
from rest_framework import serializers

from .models import Projects


class ProjectSerializer(serializers.ModelSerializer):
    """
    项目序列化
    """

    class Meta:
        model = Projects
        fields = '__all__'
