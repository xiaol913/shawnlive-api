from rest_framework import viewsets, mixins

from .models import Projects
from .serializer import ProjectSerializer

# Create your views here.


class ProjectsViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    """
    项目
    """
    queryset = Projects.objects.all()
    serializer_class = ProjectSerializer
