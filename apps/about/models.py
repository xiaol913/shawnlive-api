from django.db import models


# Create your models here.


class AboutMe(models.Model):
    """
    关于我
    """
    city = models.CharField(max_length=10, verbose_name='城市', help_text='城市')
    hobby = models.CharField(max_length=200, verbose_name='兴趣爱好', help_text='兴趣爱好')
    position = models.CharField(max_length=20, verbose_name='职位', help_text='职位')
    responsibilities = models.CharField(max_length=20, verbose_name='职责', help_text='职责')
    department = models.CharField(max_length=20, verbose_name='部门', help_text='部门')
    python = models.CharField(max_length=200, verbose_name='python技术栈', help_text='python技术栈')
    web = models.CharField(max_length=200, verbose_name='前端技术栈', help_text='前端技术栈')
    server = models.CharField(max_length=200, verbose_name='服务器技术栈', help_text='服务器技术栈')
    others = models.CharField(max_length=200, verbose_name='其他技术栈', help_text='其他技术栈')
    email = models.EmailField(verbose_name='Email', help_text='Email')
    qq = models.IntegerField(verbose_name='QQ', help_text='QQ')
    wechat = models.CharField(max_length=10, verbose_name='微信', help_text='微信')

    class Meta:
        verbose_name = '关于我'
        verbose_name_plural = verbose_name

    def __str__(self):
        return "关于我"
