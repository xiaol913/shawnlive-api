# _*_ coding=utf8 _*_
from rest_framework import serializers

from .models import AboutMe


class AboutMeSerializer(serializers.ModelSerializer):
    """
    关于我序列化
    """
    class Meta:
        model = AboutMe
        fields = '__all__'
