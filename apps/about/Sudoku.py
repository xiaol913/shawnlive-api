class Cell:
    def __init__(self, row, column):
        self.row = row
        self.column = column
        self.value = 0

    def __str__(self):
        return str(self.row) + ":" + str(self.column) + ":" + str(self.value)


class Sudoku:
    def __init__(self, sd_list):
        self.cell_array = []
        self.row_max = 9
        self.column_max = 9
        self.sd_list = sd_list

    def pre(self):
        for item_i in range(self.row_max):
            row_array = []
            for item_j in range(self.column_max):
                c = Cell(item_i, item_j)
                c.value = int(self.sd_list[item_i][item_j])
                row_array.append(c)
            self.cell_array.append(row_array)

    def set_cell_value(self, cell):
        if cell is None:
            return True
        if cell.value == 0:
            can_list = [1, 2, 3, 4, 5, 6, 7, 8, 9]
            self.block_check(can_list, cell)
            self.row_check(can_list, cell)
            self.column_check(can_list, cell)
            if len(can_list) == 0:
                return False
            for canNum in can_list:
                cell.value = canNum
                res = self.set_cell_value(self.my_next(cell))
                if res:
                    return True
            cell.value = 0
            return False
        else:
            return self.set_cell_value(self.my_next(cell))

    def my_next(self, c):
        if c.row + 1 < self.row_max:
            row = c.row + 1
            column = c.column
            return self.cell_array[row][column]
        elif c.column + 1 < self.column_max:
            row = 0
            column = c.column + 1
            return self.cell_array[row][column]
        else:
            return None

    def block_check(self, can_list, cell):
        block_row = cell.row // 3
        block_column = cell.column // 3
        for item_i in range(block_row * 3, (block_row + 1) * 3):
            for item_j in range(block_column * 3, (block_column + 1) * 3):
                c_value = self.cell_array[item_i][item_j].value
                if self.cell_array[item_i][item_j].value == 0:
                    continue
                if c_value in can_list:
                    can_list.remove(c_value)

    def row_check(self, can_list, cell):
        for item in range(self.column_max):
            c_value = self.cell_array[cell.row][item].value
            if c_value == 0:
                continue
            if c_value in can_list:
                can_list.remove(c_value)

    def column_check(self, can_list, cell):
        for item in range(self.row_max):
            c_value = self.cell_array[item][cell.column].value
            if c_value == 0:
                continue
            if c_value in can_list:
                can_list.remove(c_value)