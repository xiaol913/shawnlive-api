from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import mixins

from .models import AboutMe
from .serializer import AboutMeSerializer
from .Sudoku import Sudoku


# Create your views here.


class AboutMeViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.RetrieveModelMixin):
    """
    list:
        列表
    retrieve:
        我的详情
    """
    queryset = AboutMe.objects.all()
    serializer_class = AboutMeSerializer


def get_result(request):
    sd_str = request.GET.get('sudoku')
    sd_list = []
    index = 0
    str_temp = ''
    for item in sd_str:
        if index < 8:
            str_temp += item
            index += 1
        else:
            str_temp += item
            sd_list.append(str_temp)
            str_temp = ''
            index = 0
    sudoku = Sudoku(sd_list)
    sudoku.pre()
    sudoku.set_cell_value(sudoku.cell_array[0][0])
    result = []
    for i in range(9):
        answer = ''
        for j in range(9):
            answer += str(sudoku.cell_array[i][j].value)
        result.append(answer)
    return render(request, 'sudoku.html', {'result': result})
