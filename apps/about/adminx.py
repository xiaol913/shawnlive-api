# _*_ coding=utf8 _*_
from xadmin import site
from xadmin import sites
from xadmin import views
from .models import AboutMe


class BaseSetting(object):
    enable_themes = True
    use_bootswatch = True


class GlobalSetting(object):
    site_title = 'ShawnLive.com'
    site_footer = 'ShawnLive.com'


@sites.register(AboutMe)
class AboutMeAdmin(object):
    model_icon = 'fa fa-eercast'
    list_display = ['position', 'responsibilities', 'department', ]


site.register(views.BaseAdminView, BaseSetting)
site.register(views.CommAdminView, GlobalSetting)
